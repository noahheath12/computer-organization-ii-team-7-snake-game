///
/// @mainpage	LCD_Joystick
///
/// @details	Joystick controlled cursor
/// @n
/// @n @a		Developed with [embedXcode+](http://embedXcode.weebly.com)
///
/// @author		Rei Vilo
/// @author		http://embeddedcomputing.weebly.com
/// @date		11/12/2013 10:19
/// @version	101
///
/// @copyright	(c) Rei Vilo, 2013
/// @copyright	CC = BY SA NC
///
/// @see		ReadMe.txt for references
///


///
/// @file		LCD_Joystick.ino
/// @brief		Main sketch
///
/// @details	Joystick controlled cursor
/// @n @a		Developed with [embedXcode+](http://embedXcode.weebly.com)
///
/// @author		Rei Vilo
/// @author		http://embeddedcomputing.weebly.com
/// @date		11/12/2013 10:19
/// @version	101
///
/// @copyright	(c) Rei Vilo, 2013
/// @copyright	CC = BY SA NC
///
/// @see		ReadMe.txt for references
/// @n
///


// Core library for code-sense
#if defined(ENERGIA) // LaunchPad MSP430, Stellaris and Tiva, Experimeter Board FR5739 specific
#include "Energia.h"
#else // error
#error Platform not defined
#endif

// Prototypes


// Include application, user and local libraries
#include <SPI.h>

#include <LCD_screen.h>
#include <LCD_screen_font.h>
#include <LCD_utilities.h>
#include <Screen_HX8353E.h>
#include <Terminal12e.h>
#include <Terminal6e.h>
#include <Terminal8e.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
Screen_HX8353E myScreen;

// Define variables and constants
#define joystickX 2
#define joystickY 26

#define north 0
#define east 1
#define south 2
#define west 3

struct coord{
  uint16_t x;
  uint16_t y;
  coord * next;
  coord * prev;
  coord(){
    next = NULL;
    prev = NULL;
  }
};

coord * head = NULL;
coord * tail = NULL;

const int buzzerPin = 40;
const uint16_t topbutton  = 33;
const uint16_t bottombutton = 32;

uint16_t sdirection = south;
uint16_t foodx = 0;
uint16_t foody = 0;
uint16_t snakelength = 0;
uint16_t isEating = 0;
uint16_t foodCounter = 0;
coord snakecoordinates[1024];

uint16_t x, y, x00, y00, deltax, deltay;
uint16_t colour;
uint32_t z;

uint16_t x_cord, y_cord, x_max_num, y_max_num;
uint16_t matrix[32][32] = {
  {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
};


// Add setup code
void setup()
{
    // By default MSP432 has analogRead() set to 10 bits. 
    // This Sketch assumes 12 bits. Uncomment to line below to set analogRead()
    // to 12 bit resolution for MSP432.
    //analogReadResolution(12);
    //srand(getpid());
    //randomSeed(123412341234);
    randomSeed(analogRead(0));
    
    myScreen.setFontSolid(false);
    myScreen.setFontSize(2);
    
    pinMode(topbutton, INPUT);
    pinMode(bottombutton, INPUT);
    pinMode(buzzerPin,OUTPUT);
    
    myScreen.begin();
    
    myScreen.gText((myScreen.screenSizeX()-16*myScreen.fontSizeX())/2, 20, "Welcome To Snake");
    myScreen.gText((myScreen.screenSizeX()-16*myScreen.fontSizeX())/2, 100, "Button 2 To Turn");
    
    delay(10000);
    myScreen.clear();
    delay(1000);
    
    myScreen.setPenSolid(true);
    
    for(uint16_t i = 0; i < 32; i++)
    {
      for(uint16_t j = 0; j < 32; j++)
      {
        myScreen.dRectangle(0*4, j*4, 4, 4, greenColour);
        myScreen.dRectangle(i*4, 0*4, 4, 4, greenColour);
        myScreen.dRectangle(i*4, 31*4, 4, 4, greenColour);
        myScreen.dRectangle(31*4, j*4, 4, 4, greenColour);
      }
    }
   
    
    x00 = 0;
    y00 = 0;
    
    head = new coord;
    head->x = 16;
    head->y = 16;
    
    
    tail = new coord;
    tail -> x = 16;
    tail -> y = 15;
    
    head -> prev = NULL;
    head -> next = tail;
    tail-> prev = head;
   
    
    //init position of starting square and set position in matrix
    x = 16;
    y = 16;
    matrix[head->x/4][head->y/4] = 1;
    
    deltax = 0;
    deltay = 3;
    
    drawSnake();
    delay(2000);
    drawFood();
}

// Add loop code
void loop()
{
  //check button presses 
  if(digitalRead(topbutton) == HIGH){ //turn right
    sdirection = sdirection +1;
    if(sdirection == 4){
      sdirection = 0;
    }
  }
       
  if(digitalRead(bottombutton) == HIGH){
    sdirection = sdirection - 1;
    if(sdirection == -1){
      sdirection = 3;
    }
  }   

  char str[15];
  sprintf(str, "%d", head->y);
  myScreen.gText((myScreen.screenSizeX()-14*myScreen.fontSizeX())/2, 90, str);

  ///next square movement
   
   if(sdirection == north){
     deltax = 0; deltay = -1;
   }
   if(sdirection == east){
     deltax = 1; deltay = 0;
   }
   if(sdirection == south){
     deltax = 0; deltay = 1;
   }
   if(sdirection == west){
     deltax = -1; deltay = 0;
   }
       
   updateSnakeCoordinates();
      
   drawSnake();
       
   myScreen.dRectangle((head->x)*4, (head->y)* 4, 4, 4, yellowColour);
       

   checkFoodCollision(); // broken
   checkBorderCollision();// broken
   delay(100);
}

void drawSnake(){
  for(coord * current = head; current != NULL; current = current -> next){
    myScreen.dRectangle((current->x)*4, (current->y)* 4, 4, 4, redColour);
  }
}

void deleteSnake(){
  coord * temp = tail;
  while(temp != NULL){
    tail = tail->prev; 
    delete temp;
    temp = tail; 
  }
  foodCounter = 0;
}

void checkFoodCollision(){

  uint16_t isx = head->x - foodx;
  uint16_t isy = head->y - foody;
  char str[15];
  sprintf(str, "%d", foodCounter);
  
  myScreen.gText((myScreen.screenSizeX()-16*myScreen.fontSizeX())/2, 5, "Score = ");
  myScreen.gText((myScreen.screenSizeX()-1*myScreen.fontSizeX())/2, 5, str);
   //myScreen.gText((myScreen.screenSizeX()-9*myScreen.fontSizeX())/2, 20, "x:" + i32toa((int16_t)(head->x)) + "=" + i32toa((int16_t)(foodx)) + ":" + i32toa((int16_t)(isx)));
   //myScreen.gText((myScreen.screenSizeX()-9*myScreen.fontSizeX())/2, 30, "y:" + i32toa((int16_t)(head->y)) + "=" + i32toa((int16_t)(foody)) + ":" + i32toa((int16_t)(isy)));

 
  if(isx == 0){
   if(isy == 0){
     //myScreen.gText((myScreen.screenSizeX()-9*myScreen.fontSizeX())/2, 10, "Ur Eating");
     
     
 //    tone(buzzerPin, 500, 500);
    
    

    //
    //
    //
    //
    //
    ////////////////////////////////////////
    
    
    //snakelength = snakelength + 1;
    // redraw the food at another random position and mark that position in the matrix with a 2
    
    drawFood();
    AddBodyPart();
    foodCounter++;
    
   }     
  } else {

    //myScreen.gText((myScreen.screenSizeX()-9*myScreen.fontSizeX())/2, 10, "No Food");
    //myScreen.gText((myScreen.screenSizeX()-9*myScreen.fontSizeX())/2, 20, i32toa((int16_t)(matrix[(head->x)][(head->y)])));
    
  }
  
}


void updateSnakeCoordinates(){
  //int oldx = tail -> x;
  //int oldy = tail -> y;
  eraseTailSquare(); 
  
  
  for(coord * current = tail; current != NULL; current = current -> prev){
    coord * next = current -> prev;
    if(next != NULL){
      current -> x = next -> x;
      current -> y = next -> y;
    }
  }
   head->x = head->x + (deltax );
   head->y = head->y + (deltay );
   matrix[head->x][head->y] = matrix[head->x][head->y] + 1;
}

void randomizeFoodCoordinates(){
  bool isConflict = false;
  foodx = (random(116) % 32);
  foody = (random(116) % 32);
  isConflict = matrix[foodx][foody] == 1;
  while(isConflict){
    foodx = (random(116) % 32);
    foody = (random(116) % 32);
  }
  matrix[foodx][foody] = 1;
}

void drawFood(){
  randomizeFoodCoordinates();
  
//  myScreen.gText((myScreen.screenSizeX()-9*myScreen.fontSizeX())/2, 70, "foodx:" + i32toa((int16_t)(foodx)));
//                myScreen.gText((myScreen.screenSizeX()-9*myScreen.fontSizeX())/2, 80, "foody:" + i32toa((int16_t)(foody)));
  myScreen.dRectangle(foodx * 4, foody * 4, 4, 4, blueColour);
}
void eraseTailSquare(){
  myScreen.dRectangle((tail->x)*4, (tail->y)*4, 4, 4, blackColour);
  matrix[tail->x][tail->y] = 0;
}

void checkBorderCollision(){
 if(head->x <= 0){
    newGame(); 
 }
 if(head->x >= 30){
    newGame();
 }
 if(head->y <= 0){
    newGame();
 }
 if(head->y >= 30){
    newGame();
 }
}

void newGame(){
    deleteSnake();
    myScreen.clear();
    delay(333);
    setup();
}

void AddBodyPart()
{
  coord * temp1 = new coord;
  temp1 -> prev = tail;
  tail -> next = temp1;
  tail = temp1;
  temp1 = NULL;
  delete temp1;
  for(coord * current = tail; current != NULL; current = current -> prev){
    coord * next = current -> prev;
    if(next != NULL){
      current -> x = next -> x;
      current -> y = next -> y;
    }
  }
   head->x = head->x + (deltax );
   head->y = head->y + (deltay );
   matrix[head->x][head->y] = matrix[head->x][head->y] + 1;
}
